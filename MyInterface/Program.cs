﻿using System;

namespace MyInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            CircularLinkedList<string> list = new CircularLinkedList<string>();
            
            
            list.Add("A");
            list.Add("D");
            list.Add("C");
            list.Add("B");
            list.Add("E");

            foreach (var e in list)
            {
                Console.WriteLine(e);
            }

            list.MySorted();
            Console.WriteLine("=======");

            foreach (var e in list)
            {
                Console.WriteLine(e);
            }

            Console.ReadLine();
        }
    }
}
