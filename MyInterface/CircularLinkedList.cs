﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MyInterface
{
    public class CircularLinkedList<T> : IEnumerable<T>, IAlgorithm
    {
        Item<T> head;
        int count;

        public void Add(T data)
        {
            Item<T> item = new Item<T>(data);

            if (head == null)
            {
                head = item;
                head.Next = item;
                head.Prev = item;
            }
            else
            {
                item.Prev = head.Prev;
                item.Next = head;
                head.Prev.Next = item;
                head.Prev = item;
            }
            count++;
        }

        public void Clear()
        {
            head = null;
            count = 0;
        }

        public IEnumerator<T> GetEnumerator()
        {
            Item<T> current = head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != head);
        }
        private bool getIsChange(Item<T> item1, Item<T> item2)
        {
            if (item1.Data is string)
            {
                return String.Compare(item1.Data.ToString(), item2.Data.ToString()) > 0;
            }
            else
            {
                if (item1.Data is int)
                { return Convert.ToInt32(item1.Data) > Convert.ToInt32(item2.Data); }
                else
                { return false; }
            }
        }

        public void MyFindBinar()
        {
            throw new NotImplementedException();
        }

        public void MySorted()
        {
            Item<T> current = head;
            Item<T> next1 = head;
            Item<T> next2 = head;
            Item<T> T1Prev;
            Item<T> T2Next;
            Item<T> tmp;
            int step = 0;

             if (count > 1)
            {
                do
                {
                    
                    next1 = current;
                    next2 = next1.Next;

                     if (getIsChange(next1, next2))
                     {

                        next1.Prev.Next = next2;
                        next2.Prev = next1.Prev;
                        

                        next1.Prev = next2;
                        next1.Next = next2.Next;
                        next2.Next = next1;                      

                        step = 0;
                         /*if (next1 == head)
                        {
                            head = next2;
                        }
                        if (next2 == head)
                        {
                            head = next1;
                        }
                        current = head.Prev;*/
                    }
                    if (next1.Next.Next == head)
                        current = head;
                    else
                        current = current.Next;
                    step++;
                }
                while ((step<=count +1));
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
    }
}
